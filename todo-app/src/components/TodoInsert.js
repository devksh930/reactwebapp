import React, {useCallback, useRef, useState} from 'react';
import {MdAdd} from "react-icons/md";
import "./TodoInsert.scss";

const TodoInsert = ({onInsert}) => {
    const [value, setValue] = useState('');
    const inputEl = useRef(null);


    const submitHandler = useCallback((e) => {
        onInsert(value);
        setValue('');
        e.preventDefault();
        inputEl.current.focus();
    }, [onInsert, value]);

    return (
        < form
    className = "TodoInsert"
    onSubmit = {submitHandler} >
        < input
    ref = {inputEl}
    placeholder = "할 일을 입력하세요"
    value = {value}
    onChange = {(e)
=>
    setValue(e.target.value)
}
    />
    < button
    type = "submit" >
        < MdAdd / >
        < /button>
        < /form>
)
};
export default TodoInsert;
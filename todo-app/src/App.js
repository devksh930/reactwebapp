import React, {useCallback, useRef, useReducer} from "react";
import TodoTemplate from "./components/TodoTemplate";
import TodoInsert from "./components/TodoInsert";
import TodoList from "./components/TodoList";

const createBulkTodos = () => {
    const todos = [];
    for (let i = 1; i <= 5000; i++) {
        todos.push({
            id: i,
            text: `할 일 ${i}`,
            checked: false,
        });
    }
    return todos;
};

const todosReducer = (todos, action) => {
    switch (action.type) {
        case "INSERT": //action = {type: 'INSERT',todo:{id:... ,text:.... , chekcked:....,}}
            return todos.concat(action.todo);

        case "REMOVE":
            return todos.filter((todo) => todo.id !== action.id);

        case "TOGGLE":
            return todos.map((todo) =>
                todo.id === action.id ? {...todo, checked: !todo.checked} : todo
            );

        default:
            return todos;
    }
};

function App() {
    //   const [todos, setTodos] = useState(createBulkTodos);
    // const [todos, setTodos] = useState([
    //     {
    //         id: 1,
    //         text: "코딩이나해라",
    //         checked: true,
    //     },
    //     {
    //         id: 2,
    //         text: "코딩작업이나해라",
    //         checked: true,
    //     },
    //     {
    //          id: 3,
    //         text: "공부나해라",
    //         checked: false,
    //     }
    // ]);
    const [todos, dispatch] = useReducer(
        todosReducer,
        undefined,
        createBulkTodos
    );

    const nextId = useRef(5001);

    const insertTodo = useCallback((text) => {
        const todo = {
            id: nextId.current,
            text,
            checked: false,
        };
        dispatch({
            type: "INSERT",
            todo,
        });
    }, []);

    const removeTodo = useCallback(
        (id) => dispatch({type: "REMOVE", id: id}),
        []
    );

    const toggleTodo = useCallback(
        (id) => dispatch({type: "TOGGLE", id: id}),
        []
    );

    //   const insertTodo = useCallback((text) => {
    //     const todo = {
    //       id: nextId.current,
    //       text,
    //       checked: false,
    //     };

    //     setTodos((todo_list) => todo_list.concat(todo));
    //     nextId.current += 1;
    //   }, []);

    //   const removeTodo = useCallback((id) => {
    //     setTodos((todo_list) => todo_list.filter((todo) => todo.id !== id));
    //   }, []);

    //   const toggleTodo = useCallback((id) => {
    //     setTodos((todo_list) =>
    //       todo_list.map((todo) =>
    //         todo.id === id ? { ...todo, checked: !todo.checked } : todo
    //       )
    //     );
    //   }, []);

    return (
        < TodoTemplate >
        < TodoInsert
    onInsert = {insertTodo}
    />
    < TodoList
    todos = {todos}
    onRemove = {removeTodo}
    onToggle = {toggleTodo}
    />
    < /TodoTemplate>
)
    ;
}

export default App;
